<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('payment_process', function () {
    dd('payment');
})->name('payment_process');*/

Route::get('home' , function() {
  return 'home';
});

Route::get('dashboard', 'callbackController@dashboard')->name('dashboard');

Route::get('callback', 'callbackController@index')->name('callback');

Route::get('redirect', 'callbackController@redirect')->name('redirect');

Route::any('change_currency', 'callbackController@Currency')->name('change_currency');

/*Route::post('search', function () {
    return view('search');
})->middleware('cors')->name('search');*/

Route::post('search', 'callbackController@validatefrontend')->middleware('cors')->name('search');

Route::get('uninstall', 'callbackController@uninstall')->name('uninstall');

Route::get('payment_process', 'callbackController@payment_method')->name('payment_process');

Route::get('payment_success', 'callbackController@payment_compelete')->name('payment_success');

//Route::get('help', function () {
//    return view('help',['active' => 'help']);
//})->name('help');

Route::get('help', "HelpController@index")->name('help');

Route::post('save', 'ChristmasController@store')->name('save');

Route::post('snow', 'SnowflackController@index')->middleware('cors')->name('snow');

Route::get('intervention-resizeImage',['as'=>'intervention.getresizeimage','uses'=>'FileController@getResizeImage']);

Route::post('intervention-resizeImage',['as'=>'intervention.postresizeimage','uses'=>'FileController@postResizeImage']);

Route::any('update-modal-status', 'callbackController@update_modal_status')->name('update-modal-status'); 