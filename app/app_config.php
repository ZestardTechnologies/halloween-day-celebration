<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class app_config extends Model
{
    
    protected $table = 'appconfig';
    public $timestamps = false; 
    protected $primaryKey = 'id'; 
    protected $fillable =[ 
        'date_format',      
        'app_status',    
        'select_page',  
        'show_snow',
        'show_header_garland',
        'show_footer_garland',
        'show_santa',
        'snow_images',
        'header_garland_image',
        'footer_garland_image',
        'santa_image',
        'santa_direction',
        'show_flying_santa',
        'flying_santa_image',
        'flying_santa_direction',
        'store_id'  
        ]; 
    
}