<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShopModel;
use App\app_config;

class HelpController extends Controller {

    public function index() {
        $app_name = "Halloween's Day Celebration";
        return view('help', ['active' => 'help', 'app_name' => $app_name]);
    }

}
