<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShopModel;
use App\app_config;
class SnowflackController extends Controller
{
    public function index(Request $request)
    {
        $shop = $request['shop'];        
        //$shop_find = ShopModel::where('domain' , $shop)->first();
        $shop_find = ShopModel::where('store_name' , $shop)->first();
        $appsettings = app_config::where('store_id' , $shop_find->id)->first();
        //print_r($appsettings);
        if($appsettings){            
            $config = ['app_status' => $appsettings->app_status,'page_option' => $appsettings->select_page, 'show_snow' => $appsettings->show_snow,'show_header'=>$appsettings->show_header_garland,'show_footer'=>$appsettings->show_footer_garland,'show_santa'=>$appsettings->show_santa, 'snow_images'=>$appsettings->snow_images, 'header_garland_image'=>$appsettings->header_garland_image, 'footer_garland_image'=>$appsettings->footer_garland_image, 'santa_image'=>$appsettings->santa_image, 'santa_direction'=>$appsettings->santa_direction, 'show_flying_santa'=>$appsettings->show_flying_santa, 'flying_santa_image'=>$appsettings->flying_santa_image, 'flying_santa_direction'=>$appsettings->flying_santa_direction];
            return json_encode($config);
        } else {
            $config = ['app_status' => 1,'page_option' => 0, 'show_snow' => 1,'show_header'=> 1,'show_footer'=>1,'show_santa'=>1,'snow_images'=>'["snow25.png","snow26.png","snow27.png"]', 'header_garland_image'=>'header1.png', 'footer_garland_image'=>'footer1.png', 'santa_image'=>'halloween1.png','santa_direction'=>'bottom_left', 'show_flying_santa'=> 1, 'flying_santa_image'=>'skeleton1.gif', 'flying_santa_direction'=>'left_right'];
            return json_encode($config);
        }
    }
}
