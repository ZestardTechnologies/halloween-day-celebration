<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShopModel;
use App\app_config;

class ChristmasController extends Controller
{
        
    public function store(Request $request) {
        //dd($request);
        $shop = session('shop');
        $shop_model = new ShopModel;
        $shop_find = ShopModel::where('store_name' , $shop)->first();
        //dd($shop_find->id);
        $app_settings= new app_config;
        $get_settings = app_config::where('store_id' , $shop_find->id)->first();
        if(count($get_settings) > 0)
        {
            $get_settings->app_status = $request['app-status'];
            $get_settings->select_page = $request['select_page'];
            $get_settings->show_snow = $request['show_snow'];
            
            $snow_image = $request['snow_images'];
            if($snow_image){
                $snow_images = implode('","',$snow_image);            
                $get_settings->snow_images = '["'.$snow_images.'"]';
            } else {
                $get_settings->snow_images = false;
            }
            
            $get_settings->show_header_garland = $request['show_header_garland'];
            $get_settings->header_garland_image = $request['header_garland_image'];
            $get_settings->show_footer_garland = $request['show_footer_garland'];
            $get_settings->footer_garland_image = $request['footer_garland_image'];
            $get_settings->show_santa = $request['show_santa'];
            $get_settings->santa_direction = $request['santa_direction'];
            $get_settings->santa_image = $request['santa_image'];
            $get_settings->show_flying_santa = $request['show_flying_santa'];
            $get_settings->flying_santa_direction = $request['flying_santa_direction'];
            $get_settings->flying_santa_image = $request['flying_santa_image'];
            $get_settings->store_id = $shop_find->id;
            $get_settings->save();
                        
        }
        else{
            $app_settings->app_status = $request['app-status'];
            $app_settings->select_page = $request['select_page'];
            $app_settings->show_snow = $request['show_snow'];
            
            $snow_image = $request['snow_images'];
            if($snow_image){
                $snow_images = implode('","',$snow_image);            
                $app_settings->snow_images = '["'.$snow_images.'"]';
            } else {
                $app_settings->snow_images = false;
            }
            
            $app_settings->show_header_garland = $request['show_header_garland'];
            $app_settings->header_garland_image = $request['header_garland_image'];
            $app_settings->show_footer_garland = $request['show_footer_garland'];
            $app_settings->footer_garland_image = $request['footer_garland_image'];
            $app_settings->show_santa = $request['show_santa'];
            $app_settings->santa_direction = $request['santa_direction'];
            $app_settings->santa_image = $request['santa_image'];
            $app_settings->show_flying_santa = $request['show_flying_santa'];
            $app_settings->flying_santa_direction = $request['flying_santa_direction'];
            $app_settings->flying_santa_image = $request['flying_santa_image'];
            $app_settings->store_id = $shop_find->id;
            $app_settings->save();
            $app_settings = app_config::where('store_id' , $shop_find->id)->first();
        }
        $notification = array(
            'message' => 'Save Succesfully.',
            'alert-type' => 'success'
        );
        //dd($get_settings);
        //dd('test');
        return redirect()->back()->with('notification',$notification);
        return view ('dashboard',['store_record' => $get_settings, 'active' => 'globalset']);
     //dd($request);
     
    }


    
    
    public function edit(Request $request) {
        
    }
    
    public function update(Request $request) {
        
    }
}
