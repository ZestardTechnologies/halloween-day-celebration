@extends('header')
@section('content')
<?php
$store_name = session('shop');
?>
<div class="container">
    <div class="row">
        <div class="card">

            <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">              
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <img src="" class="imagepreview" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-content help">
                <h5>Need Help?</h5>
                <p><b>To customize any thing within the app or for other work just contact us on below details</b></p>
                <ul>
                    <li>Developer: <b><a target="_blank" href="https://www.zestardshop.com">Zestard Technologies Pvt Ltd</a></b></li>
                    <li>Email: <b><a href="mailto:support@zestard.com">support@zestard.com</a></b></li>
                    <li>Website: <b><a target="_blank" href="https://www.zestardshop.com">https://www.zestardshop.com</a></b></li>
                </ul>
                <hr>

                <h5>General Instruction</h5>
                <h6><b>App Configuration:</b></h6>
                <ul class="limit">
                    <li>After Installing the App, It will show <a href = "{{ url('dashboard') }}">dashboard page</a>, where store owner can Customize {{ $app_name }} app.</li>
                    <li>On dashboard, owner can able to enable and disable the app.</li>
                    <li>{{ $app_name }} can show snow on only home page and on the entire website.</li>
                    <li>{{ $app_name }} can show Header Garland on only home page and on the entire website.</li>
                    <li>{{ $app_name }} can show Footer Garland on only home page and on the entire website.</li>
                    <li>{{ $app_name }} can show Halloween's Day Celebration Image on only home page and on the entire website.</li>						
                </ul>
                <h6><b>Uninstall App:</b></h6>
                <ul class="limit">
                    <li>To remove the App, go to <a href="https://<?php echo $store_name; ?>/admin/apps" target="_blank"><b>Apps</b></a>.</li>
                    <li>Click on delete icon of {{ $app_name }} App.</li>
                    <li>If possible then remove shortcode where you have pasted.</li>
                </ul>
                <hr>

                <h5>Follow the Steps:</h5>
                <h6><b>Where to paste shortcode?</b></h6>
                <ul class="limit">						
                    <li>After saving the details, you can copy the <b><a href = "{{ url('dashboard') }}">Shortcode</a></b> from the schortcode section.</li>

                    <li>You can paste the shortcode by clicking on <a href="https://<?php echo $store_name; ?>/admin/themes/current/?key=layout/theme.liquid" target="_blank"><b>theme.liquid</b></a></li>
                </ul>

                <a class="goback" href="{{ url('dashboard') }}">
                    <img src="{!! asset('image/back.png') !!}">Go Back
                </a>
            </div>				
        </div>
    </div>
</div>
@endsection
<style>
    .help h5{
        font-size: 24px;
    }
    .help h6{
        font-size: 16px;
    }
    .help a{
        color: #039be5;
    }
    .help ul{
        padding-left: 0px;
    }
    .limit {
        margin-left: 20px;
    }
    ul.limit li {
        list-style-type: disc !important;
        display: list-item;
    }
</style>