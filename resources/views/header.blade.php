@yield('header')
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Halloween Day Celebration</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- toastr CSS -->
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    <link rel="stylesheet" href="{{ asset('css/spectrum.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    
    <!-- Bootstrap core CSS -->
    <!-- <link rel="stylesheet" href="{{ asset('css/materialize.min.css') }}"> -->
    
    <!-- magnificent popup CSS -->
    <!-- <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}"> -->
    
    <!-- jquery datatable CSS -->
    <link rel="stylesheet" href="{{ asset('css/datatable/dataTables.bootstrap.min.css') }}">
   
    <!-- custom CSS -->
    <!-- <link rel="stylesheet" href="{{ asset('css/style.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/christmas-snowflake-custom.css') }}">
     
    <!-- flag CSS -->
    <!--<link rel="stylesheet" href="{{ asset('css/phoca-flags.css') }}">-->
    
    <!-- shopify Script for fast load -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    
    <!-- jquery datatable js -->
    <script src="{{ asset('js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatable/dataTables.bootstrap.min.js') }}"></script>
    <script>
      $(document).ready(function() {
          $('#productlist').DataTable({
            "paging":   true,
            "ordering": true
          });
        });
    </script>
    
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script>
      ShopifyApp.init({
        apiKey: 'f41539a099037d5c352b88fdbb3148fd',
        shopOrigin: '<?php echo "https://".session('shop'); ?>'
      });

      ShopifyApp.ready(function() {
          ShopifyApp.Bar.initialize({
            icon: "",
            title: '',
            buttons: {}
          });
        });
    </script>
    <style>
        .review-div {
            text-align: center !important;
            width: 100%;
            background-color: #fafad0;
            padding: 10px;
            box-shadow: 1px 1px 2px #777;
            display: inline-block;
            position: sticky;
            top: 0px;
            left: 0px;
            z-index: 100;
         }
    </style>
  </head>

  <body>

@yield('navigation')
<?php if(!isset($active)){$active="";} ?>
<div class="review-div">
    Are you loving this app? Please spend 2mins to help us <a href="https://apps.shopify.com/halloween-day-celebration?reveal_new_review=true" target="_blank">write a review</a>. 
</div>
<div class="navbar">
    <nav class="christmas-snowflake-nav">
      <div class="nav-wrapper">
        <div class="">
          <div class="col-md-12">
            <p class="brand-logo center">
                <a href="{{ url('/dashboard') }}" class="menu-matrix">Halloween's Day Celebration</a>                
            </p>            
          </div>
        </div>
      </div>
    </nav>
  </div>

@yield('content')
<!-- <script src="{{ asset('js/materialize.min.js') }}"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/spectrum.js') }}"></script>
<script src="{{ asset('js/jquery.copy-to-clipboard.js') }}"></script>
<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('js/javascript.js') }}"></script>


<script>
  @if(Session::has('notification'))
    var type = "{{ Session::get('notification.alert-type', 'info') }}";
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('notification.message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('notification.message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('notification.message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('notification.message') }}");
            break;         
        case 'options':
            toastr.warning("{{ Session::get('notification.message') }}");
            break;

    }
  @endif
</script>
<script type="text/javascript">
  jQuery(".showInputLeftHeader").spectrum({
    preferredFormat: "hex",
    showInput: true,
    showAlpha: true,
    color: "#96a2a0",
    palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
  });

  jQuery(".showInputTopHeader").spectrum({
    preferredFormat: "hex",
    showInput: true,
    showAlpha: true,
    color: "#5c4040",
    palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
  });

  jQuery(".showInputLeftText").spectrum({
    preferredFormat: "hex",
    showInput: true,
    showAlpha: true,
    color: "#fff",
    palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
  });

  jQuery(".showInputTopText").spectrum({
    preferredFormat: "hex",
    showInput: true,
    showAlpha: true,
    color: "#fff",
    palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
  });

  jQuery(".showInputBroder").spectrum({
    preferredFormat: "hex",
    showInput: true,
    showAlpha: true,
    color: "#000",
    palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
  });

  jQuery(".showInputQuantity").spectrum({
    preferredFormat: "hex",
    showInput: true,
    showAlpha: true,
    color: "#289b28",
    palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
  });

  jQuery(".showInputPrice").spectrum({
    preferredFormat: "hex",
    showInput: true,
    showAlpha: true,
    color: "#f00",
    palette: [["red", "rgba(0, 255, 0, .5)", "rgb(0, 0, 255)"]]
  });
</script>

<script type="text/javascript">
      function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        //alert('Copied!');
        $temp.remove();
      }     
    </script>

    <script>
      jQuery(document).ready(function(){       
        jQuery(".copyMe").click(function (){
          var count = jQuery('.show').length;
          if(count == 0){
            jQuery(".show").show();
            jQuery(".success-copied").after('<div class="alert alert-success alert-dismissable show"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Success!</strong> Your shortcode has been copied.</div>');
          }
        });
      });
</script>

<script type="text/javascript">
jQuery(function() {
  jQuery('.screenshot').on('click', function() {
    jQuery('.imagepreview').attr('src', jQuery(this).attr('image-src'));
    jQuery('#imagemodal').modal('show');   
  });     
});
</script> 
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a2e20e35d3202175d9b7782/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
