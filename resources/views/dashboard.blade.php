@extends('header')
@section('content')
<?php 
    $store_name = session('shop');
?>

<script type="text/javascript">
  ShopifyApp.ready(function(){
    ShopifyApp.Bar.initialize({
      buttons: {
        primary: {
          label: 'SAVE SETTINGS',
          message: 'form_submit',
          loading: true,
        },
        secondary: {
          label: 'HELP',
          href : '{{ url('/help') }}',
          loading: false
        }
      }
    });
  });
</script>

<div class="dashboard">
    <div class="christmas-snowflake-container">
        <!-- snow settings -->
        <form id="christmas-snowflake" method="post" data-shopify-app-submit="form_submit" data-toggle="validator" action="{{ url('save') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
        <div class="col-md-6 left-christmas-snowflake">
            <div class="left christmas-snowflake">
                <h2 class="sub-heading">App Settings</h2>                    
                <div class="col-md-12 form-group section-group">
                    <div class="col-md-6 form-group">
                        <label for="app-status">App Enabled?</label>
                        <select class="form-control" id="app-status" name="app-status">
                            <option @if(count($store_record)> 0) value="1"
                                <?php echo ($store_record->app_status == '1' ? ' selected' : '') ?>@else value="1" @endif>Enabled</option>
                            <option @if(count($store_record)> 0) value="0"
                                <?php echo ($store_record->app_status == '0' ? ' selected' : '') ?>@else value="0" @endif>Disabled</option>
                        </select>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="select_page">Select Page?</label>
                        <select class="form-control" id="select_page" name="select_page">
                            <option @if(count($store_record)> 0) value="0"
                            <?php echo ($store_record->select_page == '0' ? ' selected' : '') ?>@else value="0" @endif>Home Page</option>
                            <option @if(count($store_record)> 0) value="1"
                            <?php echo ($store_record->select_page == '1' ? ' selected' : '') ?>@else value="1" @endif>All Page</option>
                        </select>
                    </div>                       
                </div>
            </div>
            
            <div class="left christmas-snowflake">
                <h2 class="sub-heading">Halloween Day Theme Settings</h2>
                <div class="col-md-12 form-group section-group">                         
                    <div class="col-md-6 form-group">
                        <label for="show_snow">Show Pumpkin?</label>
                        <select class="form-control" id="show_snow" name="show_snow">
                            <option @if(count($store_record)> 0) value="1" 
                                <?php echo ($store_record->show_snow == '1' ? ' selected' : '') ?>@else value="1" @endif>Yes</option>
                            <option @if(count($store_record)> 0) value="0" 
                                <?php echo ($store_record->show_snow == '0' ? ' selected' : '') ?>@else value="0" @endif>No</option> 
                        </select>
                    </div>
                        
                    <div class="col-md-6 form-group">
                        <label for="snow_images">Select Pumpkin Image</label>                            
                        <?php
                            if(count($store_record)> 0){
                                $image_data = $store_record->snow_images;                                
                                if($image_data == FALSE){                                    
                                    $snow_images = array();                                    
                                } else {
                                    $snow_images = json_decode($image_data);
                                }                                                                
                            }
                        ?>
                        <select id="snow_images" name="snow_images[]" class="form-control" size="7" multiple="multiple">                               
                        <!--<option class="zt-snow-image" @if(count($store_record)> 0) value="snow1.png" style="background:url('image/snow1.png') no-repeat;" <?php echo in_array("snow1.png" , $snow_images) ? ' selected' : '' ?>@else value="snow1.png" style="background:url('image/snow1.png') no-repeat;" selected @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow2.png" style="background:url('image/snow2.png') no-repeat;" <?php echo in_array("snow2.png" , $snow_images) ? ' selected' : '' ?>@else value="snow2.png" style="background:url('image/snow2.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow3.png" style="background:url('image/snow3.png') no-repeat;" <?php echo in_array("snow3.png" , $snow_images) ? ' selected' : '' ?>@else value="snow3.png" style="background:url('image/snow3.png') no-repeat;" selected @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow4.png" style="background:url('image/snow4.png') no-repeat;" <?php echo in_array("snow4.png" , $snow_images) ? ' selected' : '' ?>@else value="snow4.png" style="background:url('image/snow4.png') no-repeat;" selected @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow5.png" style="background:url('image/snow5.png') no-repeat;" <?php echo in_array("snow5.png" , $snow_images) ? ' selected' : '' ?>@else value="snow5.png" style="background:url('image/snow5.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow6.png" style="background:url('image/snow6.png') no-repeat;" <?php echo in_array("snow6.png" , $snow_images) ? ' selected' : '' ?>@else value="snow6.png" style="background:url('image/snow6.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow7.png" style="background:url('image/snow7.png') no-repeat;" <?php echo in_array("snow7.png" , $snow_images) ? ' selected' : '' ?>@else value="snow7.png" style="background:url('image/snow7.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow8.png" style="background:url('image/snow8.png') no-repeat;" <?php echo in_array("snow8.png" , $snow_images) ? ' selected' : '' ?>@else value="snow8.png" style="background:url('image/snow8.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow9.png" style="background:url('image/snow9.png') no-repeat;" <?php echo in_array("snow9.png" , $snow_images) ? ' selected' : '' ?>@else value="snow9.png" style="background:url('image/snow9.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow10.png" style="background:url('image/snow10.png') no-repeat;" <?php echo in_array("snow10.png" , $snow_images) ? ' selected' : '' ?>@else value="snow10.png" style="background:url('image/snow10.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow11.png" style="background:url('image/snow11.png') no-repeat;" <?php echo in_array("snow11.png" , $snow_images) ? ' selected' : '' ?>@else value="snow11.png" style="background:url('image/snow11.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow12.png" style="background:url('image/snow12.png') no-repeat;" <?php echo in_array("snow12.png" , $snow_images) ? ' selected' : '' ?>@else value="snow12.png" style="background:url('image/snow12.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow13.png" style="background:url('image/snow13.png') no-repeat;" <?php echo in_array("snow13.png" , $snow_images) ? ' selected' : '' ?>@else value="snow13.png" style="background:url('image/snow13.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow14.png" style="background:url('image/snow14.png') no-repeat;" <?php echo in_array("snow14.png" , $snow_images) ? ' selected' : '' ?>@else value="snow14.png" style="background:url('image/snow14.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow15.png" style="background:url('image/snow15.png') no-repeat;" <?php echo in_array("snow15.png" , $snow_images) ? ' selected' : '' ?>@else value="snow15.png" style="background:url('image/snow15.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow16.png" style="background:url('image/snow16.png') no-repeat;" <?php echo in_array("snow16.png" , $snow_images) ? ' selected' : '' ?>@else value="snow16.png" style="background:url('image/snow16.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow17.png" style="background:url('image/snow17.png') no-repeat;" <?php echo in_array("snow17.png" , $snow_images) ? ' selected' : '' ?>@else value="snow17.png" style="background:url('image/snow17.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow18.png" style="background:url('image/snow18.png') no-repeat;" <?php echo in_array("snow18.png" , $snow_images) ? ' selected' : '' ?>@else value="snow18.png" style="background:url('image/snow18.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow19.png" style="background:url('image/snow19.png') no-repeat;" <?php echo in_array("snow19.png" , $snow_images) ? ' selected' : '' ?>@else value="snow19.png" style="background:url('image/snow19.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow20.png" style="background:url('image/snow20.png') no-repeat;" <?php echo in_array("snow20.png" , $snow_images) ? ' selected' : '' ?>@else value="snow20.png" style="background:url('image/snow20.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow21.png" style="background:url('image/snow21.png') no-repeat;" <?php echo in_array("snow21.png" , $snow_images) ? ' selected' : '' ?>@else value="snow21.png" style="background:url('image/snow21.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow22.png" style="background:url('image/snow22.png') no-repeat;" <?php echo in_array("snow22.png" , $snow_images) ? ' selected' : '' ?>@else value="snow22.png" style="background:url('image/snow22.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow23.png" style="background:url('image/snow23.png') no-repeat;" <?php echo in_array("snow23.png" , $snow_images) ? ' selected' : '' ?>@else value="snow23.png" style="background:url('image/snow23.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow24.png" style="background:url('image/snow24.png') no-repeat;" <?php echo in_array("snow24.png" , $snow_images) ? ' selected' : '' ?>@else value="snow24.png" style="background:url('image/snow24.png') no-repeat;" @endif></option>-->
                            
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow25.png" style="background:url('image/snow25.png') no-repeat;" <?php echo in_array("snow25.png" , $snow_images) ? ' selected' : '' ?>@else value="snow25.png" style="background:url('image/snow25.png') no-repeat;" selected @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow26.png" style="background:url('image/snow26.png') no-repeat;" <?php echo in_array("snow26.png" , $snow_images) ? ' selected' : '' ?>@else value="snow26.png" style="background:url('image/snow26.png') no-repeat;" selected @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow27.png" style="background:url('image/snow27.png') no-repeat;" <?php echo in_array("snow27.png" , $snow_images) ? ' selected' : '' ?>@else value="snow27.png" style="background:url('image/snow27.png') no-repeat;" selected @endif></option>
                            
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow28.png" style="background:url('image/snow28.png') no-repeat;" <?php echo in_array("snow28.png" , $snow_images) ? ' selected' : '' ?>@else value="snow28.png" style="background:url('image/snow28.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow29.png" style="background:url('image/snow29.png') no-repeat;" <?php echo in_array("snow29.png" , $snow_images) ? ' selected' : '' ?>@else value="snow29.png" style="background:url('image/snow29.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow30.png" style="background:url('image/snow30.png') no-repeat;" <?php echo in_array("snow30.png" , $snow_images) ? ' selected' : '' ?>@else value="snow30.png" style="background:url('image/snow30.png') no-repeat;" @endif></option>

                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow31.png" style="background:url('image/snow31.png') no-repeat;" <?php echo in_array("snow31.png" , $snow_images) ? ' selected' : '' ?>@else value="snow31.png" style="background:url('image/snow31.png') no-repeat;" @endif></option>
                            
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow32.png" style="background:url('image/snow32.png') no-repeat;" <?php echo in_array("snow32.png" , $snow_images) ? ' selected' : '' ?>@else value="snow32.png" style="background:url('image/snow32.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow33.png" style="background:url('image/snow33.png') no-repeat;" <?php echo in_array("snow33.png" , $snow_images) ? ' selected' : '' ?>@else value="snow33.png" style="background:url('image/snow33.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow34.png" style="background:url('image/snow34.png') no-repeat;" <?php echo in_array("snow34.png" , $snow_images) ? ' selected' : '' ?>@else value="snow34.png" style="background:url('image/snow34.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow35.png" style="background:url('image/snow35.png') no-repeat;" <?php echo in_array("snow35.png" , $snow_images) ? ' selected' : '' ?>@else value="snow35.png" style="background:url('image/snow35.png') no-repeat;" @endif></option>
                            
                            
                        </select>                            
                    </div>
                </div>
            </div>
            
            <div class="left christmas-snowflake">
                <h2 class="sub-heading">Halloween's Day Image Settings</h2>
                    <div class="col-md-12 form-group section-group">                         
                        <div class="col-md-6 form-group">
                            <label for="show_santa">Show Halloween Image?</label>
                            <select class="form-control" id="show_santa" name="show_santa">
                                <option @if(count($store_record)> 0) value="1" 
                                    <?php echo ($store_record->show_santa == '1' ? ' selected' : '') ?>@else value="1" @endif>Yes</option>
                                <option @if(count($store_record)> 0) value="0" 
                                    <?php echo ($store_record->show_santa == '0' ? ' selected' : '') ?>@else value="0" @endif>No</option>
                            </select>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <label for="santa_direction">Halloween Image Direction</label>
                            <select class="form-control" id="santa_direction" name="santa_direction">
                                <option @if(count($store_record)> 0) value="bottom_left" 
                                    <?php echo ($store_record->santa_direction == 'bottom_left' ? ' selected' : '') ?>@else value="bottom_left" @endif>Bottom Left</option>
                                <option @if(count($store_record)> 0) value="bottom_right" 
                                    <?php echo ($store_record->santa_direction == 'bottom_right' ? ' selected' : '') ?>@else value="bottom_right" @endif>Bottom Right</option>
                                <option @if(count($store_record)> 0) value="top_left" 
                                    <?php echo ($store_record->santa_direction == 'top_left' ? ' selected' : '') ?>@else value="top_left" @endif>Top Left</option>
                                <option @if(count($store_record)> 0) value="top_right" 
                                    <?php echo ($store_record->santa_direction == 'top_right' ? ' selected' : '') ?>@else value="top_right" @endif>Top Right</option>
                            </select>
                        </div>
                        
                        <div class="col-md-12 form-group">
                            <label for="santa_image" class="zt-christmas">Select Halloween Image</label>
                            <ul class="zt-ul-li">
                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="santa_image" id="santa1_image" @if(count($store_record)> 0) value="halloween1.png" <?php echo ($store_record->santa_image == 'halloween1.png' ? ' checked' : '') ?>@else value="halloween1.png" checked @endif/>
                                        <label for="santa1_image"><img src="{!! asset('image/halloween1.png') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>

                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="santa_image" id="santa2_image" @if(count($store_record)> 0) value="halloween2.png" <?php echo ($store_record->santa_image == 'halloween2.png' ? ' checked' : '') ?>@else value="halloween2.png" @endif/>
                                        <label for="santa2_image"><img src="{!! asset('image/halloween2.png') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>
                                
                                
                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="santa_image" id="santa3_image" @if(count($store_record)> 0) value="halloween3.png" <?php echo ($store_record->santa_image == 'halloween3.png' ? ' checked' : '') ?>@else value="halloween3.png" @endif/>
                                        <label for="santa3_image"><img src="{!! asset('image/halloween3.png') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>
                                
                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="santa_image" id="santa4_image" @if(count($store_record)> 0) value="halloween4.png" <?php echo ($store_record->santa_image == 'halloween4.png' ? ' checked' : '') ?>@else value="halloween4.png" @endif/>
                                        <label for="santa4_image"><img src="{!! asset('image/halloween4.png') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
        </div>
       
        <div class="col-md-6 left-christmas-snowflake">
            <div class="right christmas-snowflake">
                <h2 class="sub-heading">Shortcode</h2>
                <div class="success-copied"></div>
                <div class="view-shortcode">
                    <textarea id="ticker-shortcode" rows="2" class="form-control short-code"  readonly="">{% include 'halloween-day-celebration' %}</textarea>
                    <button type="button" onclick="copyToClipboard('#ticker-shortcode')" class="btn tooltipped tooltipped-s copyMe" style="display: block;">                        <i class="fa fa-check"></i>Copy to clipboard</button>
                </div>
                    
                <h5 class="sub-heading follow">Follow the Steps:</h5>
                <h6><b>Where to paste shortcode?</b></h6>
                    <ul class="limit" style="list-style: disc;">						
                        <li>After saving the details, you can copy the <b>Shortcode</b> from the options.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/shortcode_paste.png') }}"><b> See Example</b></a></li>
                        <li>After copy the short-code just open the <a href="https://<?php echo $store_name;?>/admin/themes/current/?key=layout/theme.liquid" target="_blank"><b>theme.liquid</b></a> and paste that code in anywhere into the page. <a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/shortcode3.png') }}"><b>See Example</b></a></li>
                    </ul>
            </div>
                
                <div class="right christmas-snowflake">
                    <h2 class="sub-heading">Header Settings</h2>                
                <div class="col-md-12 form-group section-group">
                    <div class="col-md-12 form-group">
                        <label for="show_header_garland">Show Header Garland?</label>
                        <select class="form-control zt-block-size" id="show_header_garland" name="show_header_garland">
                            <option @if(count($store_record)> 0) value="1" 
                                <?php echo ($store_record->show_header_garland == '1' ? ' selected' : '') ?>@else value="1" @endif>Yes</option>
                            <option @if(count($store_record)> 0) value="0" 
                                <?php echo ($store_record->show_header_garland == '0' ? ' selected' : '') ?>@else value="0" @endif>No</option>
                        </select>
                    </div>
                     
                    <div class="col-md-12 form-group">
                        <label for="header_garland_image" class="zt-christmas">Select Header Image</label>
                        <ul class="zt-ul-li">
                            <li>
                                <div class="form-group">
                                    <input type="radio" name="header_garland_image" id="header1_garland_image" @if(count($store_record)> 0) value="header1.png" <?php echo ($store_record->header_garland_image == 'header1.png' ? ' checked' : '') ?>@else value="header1.png" checked @endif/>
                                    <label for="header1_garland_image"><img src="{!! asset('image/header1.png') !!}" class="zt-christmas-image"/></label>
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <input type="radio" name="header_garland_image" id="header2_garland_image" @if(count($store_record)> 0) value="header2.png" <?php echo ($store_record->header_garland_image == 'header2.png' ? ' checked' : '') ?>@else value="header2.png" @endif/>
                                    <label for="header2_garland_image"><img src="{!! asset('image/header2.png') !!}" class="zt-christmas-image"/></label>
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <input type="radio" name="header_garland_image" id="header3_garland_image" @if(count($store_record)> 0) value="header3.png" <?php echo ($store_record->header_garland_image == 'header3.png' ? ' checked' : '') ?>@else value="header3.png" @endif/>
                                    <label for="header3_garland_image"><img src="{!! asset('image/header3.png') !!}" class="zt-christmas-image"/></label>
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <input type="radio" name="header_garland_image" id="header4_garland_image" @if(count($store_record)> 0) value="header4.png" <?php //echo ($store_record->header_garland_image == 'header4.png' ? ' checked' : '') ?>@else value="header4.png" @endif/>
                                    <label for="header4_garland_image"><img src="{!! asset('image/header4.png') !!}" class="zt-christmas-image"/></label>
                                </div>
                            </li>
                            
                    </ul>
                    </div>
                </div>
            </div>
            
            <div class="right christmas-snowflake">
                 <h2 class="sub-heading">Footer Settings</h2>                
                <div class="col-md-12 form-group section-group">
                    <div class="col-md-12 form-group">
                        <label for="show_footer_garland">Show Footer Garland</label>
                        <select class="form-control zt-block-size" id="show_footer_garland" name="show_footer_garland">
                            <option @if(count($store_record)> 0) value="1" 
                                <?php echo ($store_record->show_footer_garland == '1' ? ' selected' : '') ?>@else value="1" @endif>Yes</option>
                            <option @if(count($store_record)> 0) value="0" 
                                <?php echo ($store_record->show_footer_garland == '0' ? ' selected' : '') ?>@else value="0" @endif>No</option>                                      </select>
                    </div>                 
                    
                    <div class="col-md-12 form-group">
                        <label for="footer_garland_image" class="zt-christmas">Select Footer Image</label>
                        <ul class="zt-ul-li">
                            <li>
                                <div class="form-group">
                                    <input type="radio" name="footer_garland_image" id="footer1_garland_image" @if(count($store_record)> 0) value="footer1.png" <?php echo ($store_record->footer_garland_image == 'footer1.png' ? ' checked' : '') ?>@else value="footer1.png" checked @endif/>
                                    <label for="footer1_garland_image"><img src="{!! asset('image/footer1.png') !!}" class="zt-christmas-image"/></label>
                                </div>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="right christmas-snowflake">                
                <h2 class="sub-heading">Moving Skeleton Settings</h2>
                    <div class="col-md-12 form-group section-group">                         
                        <div class="col-md-6 form-group">
                            <label for="show_flying_santa">Show Moving Skeleton?</label>
                            <select class="form-control" id="show_flying_santa" name="show_flying_santa">
                                <option @if(count($store_record)> 0) value="1" 
                                    <?php echo ($store_record->show_flying_santa == '1' ? ' selected' : '') ?>@else value="1" @endif>Yes</option>
                                <option @if(count($store_record)> 0) value="0" 
                                    <?php echo ($store_record->show_flying_santa == '0' ? ' selected' : '') ?>@else value="0" @endif>No</option>
                            </select>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <label for="flying_santa_direction">Moving Skeleton Direction</label>
                            <select class="form-control" id="flying_santa_direction" name="flying_santa_direction">
                                <option @if(count($store_record)> 0) value="right_left" 
                                    <?php echo ($store_record->flying_santa_direction == 'right_left' ? ' selected' : '') ?>@else value="right_left" @endif>Right To Left</option>
                                <option @if(count($store_record)> 0) value="left_right" 
                                    <?php echo ($store_record->flying_santa_direction == 'left_right' ? ' selected' : '') ?>@else value="left_right" @endif>Left To Right</option>
                            </select>
                        </div>
                        
                        <div class="col-md-12 form-group">
                            <label for="flying_santa_image" class="zt-christmas">Select Moving Skeleton Image</label>
                            <ul class="zt-ul-li">
                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="flying_santa_image" id="flying1_santa_image" @if(count($store_record)> 0) value="skeleton1.gif" <?php echo ($store_record->flying_santa_image == 'skeleton1.gif' ? ' checked' : '') ?>@else value="skeleton1.gif" checked @endif/>
                                        <label for="flying1_santa_image"><img src="{!! asset('image/skeleton1.gif') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>

<!--                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="flying_santa_image" id="flying2_santa_image" @if(count($store_record)> 0) value="flysanta2.gif" <?php //echo ($store_record->flying_santa_image == 'flysanta2.gif' ? ' checked' : '') ?>@else value="flysanta2.gif" @endif/>
                                        <label for="flying2_santa_image"><img src="{!! asset('image/flysanta2.gif') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>-->

<!--                                <li>
                                    <div class="form-group">                                
                                        <input type="radio" name="flying_santa_image" id="flying3_santa_image" @if(count($store_record)> 0) value="flysanta3.gif" <?php //echo ($store_record->flying_santa_image == 'flysanta3.gif' ? ' checked' : '') ?>@else value="flysanta3.gif" @endif/>
                                        <label for="flying3_santa_image"><img src="{!! asset('image/flysanta3.gif') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>-->

<!--                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="flying_santa_image" id="flying4_santa_image" @if(count($store_record)> 0) value="flysanta4.gif" <?php //echo ($store_record->flying_santa_image == 'flysanta4.gif' ? ' checked' : '') ?>@else value="flysanta4.gif" @endif/>
                                        <label for="flying4_santa_image"><img src="{!! asset('image/flysanta4.gif') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>-->

<!--                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="flying_santa_image" id="flying5_santa_image" @if(count($store_record)> 0) value="flysanta5.gif" <?php //echo ($store_record->flying_santa_image == 'flysanta5.gif' ? ' checked' : '') ?>@else value="flysanta5.gif" @endif/>
                                        <label for="flying5_santa_image"><img src="{!! asset('image/flysanta5.gif') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>-->
                            </ul>
                        </div>
                    </div>                
            </div>
                                    
        </div>       
    </form>
    </div>  
</div>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog" style="width: 80%;">
       <div class="modal-content">              
        <div class="modal-body">
         <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">×</span>
          <span class="sr-only">Close</span>
         </button>
         <img src="" class="imagepreview" style="width: 100%;">
        </div>
       </div>
      </div>
     </div>
     <div class="modal fade" id="new_note">
	<div class="modal-dialog">          
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><b>Note</b></h4>
			</div>
			<div class="modal-body">
				<p>Welcome to the Halloween Day Celebration App...!</p></br>
<p>Halloween Day Celebration consists of store decorative features which will decorate your shopify store with halloween effects.</p></br> 
<p>Our application works based on javascript only, there may be a conflict between application code and your theme (other installed applications or conflict with other scripts).Please contact on support e-mail (<a href="mailto:support@zestard.com">support@zestard.com</a>) before you try for multiple time <strong style="background-color:yellow;">install-uninstall</strong> process.
</p>
			</div>        
			<div class="modal-footer">			
				<div class="datepicker_validate" id="modal_div">
					<div>
						<strong>Show me this again</strong>
							<span class="onoff"><input name="modal_status" type="checkbox" checked id="dont_show_again"/>							
							<label for="dont_show_again"></label></span>
					</div>      
				</div>      
			</div>      
		</div>
	</div>
</div>

<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$("#dont_show_again").change(function(){
		var checked   = $(this).prop("checked");
		var shop_name = "{{ session('shop') }}";			
		if(!checked)
		{
			$.ajax({
				url:'update-modal-status',
				data:{shop_name:shop_name},
				async:false,					
				success:function(result)
				{
					
				}
			});				
			$('#new_note').modal('toggle');
		}
	});
	if("{{ $new_install }}")
	{
		var new_install = "{{ $new_install }}";	
		if(new_install == "Y")
		{
			$('#new_note').modal('show');
		}
	}		
</script>
@endsection
